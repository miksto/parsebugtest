package com.slagkryssaren.parsebugtest;

import android.app.Application;

/**
 * Created by miksto on 28/11/14.
 */
public class App extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        ParseUtil.initialize(this);
    }
}
