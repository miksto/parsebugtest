package com.slagkryssaren.parsebugtest;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class MainActivity extends Activity {

    private static final String TAG = MainActivity.class.getSimpleName();
    @InjectView(R.id.but_save_user_1)
    Button saveUserBut1;

    @InjectView(R.id.but_save_user_2)
    Button saveUserBut2;

    @InjectView(R.id.but_save_user_3)
    Button saveUserBut3;

    @InjectView(R.id.but_login_user_1)
    Button loginUserBut1;

    @InjectView(R.id.but_login_user_2)
    Button loginUserBut2;

    @InjectView(R.id.but_create_and_save_new_object)
    Button createAndSaveNewObjectBut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        View.OnClickListener onSaveUserClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseUtil.saveUser();
            }
        };

        View.OnClickListener onLoginUserClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseUtil.loginNewAnonymousUser();
            }
        };


        saveUserBut1.setOnClickListener(onSaveUserClickListener);
        saveUserBut2.setOnClickListener(onSaveUserClickListener);
        saveUserBut3.setOnClickListener(onSaveUserClickListener);

        loginUserBut1.setOnClickListener(onLoginUserClickListener);
        loginUserBut2.setOnClickListener(onLoginUserClickListener);

        createAndSaveNewObjectBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseUtil.createAndSaveNewObject();
            }
        });
    }
}
