package com.slagkryssaren.parsebugtest;

import android.content.Context;
import android.util.Log;

import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseAnonymousUtils;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;


/**
 * Created by miksto on 27/10/14.
 */
public class ParseUtil {
    private static final String TAG = ParseUtil.class.getSimpleName();

    public static final String PARSE_DEBUG_APPLICATION_ID = "DYKkU4uEdO84VKoicRuBjl36NviPoTFchQhcqZ90";
    public static final String PARSE_DEBUG_CLIENT_KEY = "46njC9gqPDhOmEqNXNiPuNBIZqSC8sxZqZBHyPs8";

    public static void initialize(Context context) {
        Parse.enableLocalDatastore(context);
        if (PARSE_DEBUG_APPLICATION_ID == null || PARSE_DEBUG_CLIENT_KEY == null) {
            throw new RuntimeException("You must provide an application id and a client key");
        }
        Parse.initialize(context, PARSE_DEBUG_APPLICATION_ID, PARSE_DEBUG_CLIENT_KEY);
    }

    public static void loginNewAnonymousUser() {
        ParseAnonymousUtils.logIn(new LogInCallback() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {
                if (e != null) {
                    Log.e(TAG, "Failed to login anonymous user", e);
                } else {
                    Log.i(TAG, "Sucessfully logged in anonymous user");
                }
            }
        });
    }

    public static void saveUser() {
        ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e != null) {
                    Log.e(TAG, "Failed to save user in background", e);
                } else {
                    Log.i(TAG, "Successfully saved currentUser in background");
                }
            }
        });
    }

    public static void createAndSaveNewObject() {
        ParseObject object = ParseObject.create("TestClass");
        object.put("userKey", ParseUser.getCurrentUser());
        object.saveEventually(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e != null) {
                    Log.e(TAG, "Failed to save object eventually", e);
                } else {
                    Log.i(TAG, "Successfully saved object eventually");
                }
            }
        });
    }

}
